/*

	advalg.h	-	made by Eduard Lahl

	This library provides a class that allows you to write to and read from
	file that is encrypted with Advanced Encrypting Algorithm

	API:

		class AdvancedEncrypt : public EncryptBase, public DecryptBase;

*/
#ifndef _ADVANCED_ALGORITHM_ENCRYPT_H_
#define _ADVANCED_ALGORITHM_ENCRYPT_H_

#include "basicutilsdll.h"
#include "encryptiondll.h"
#include "rotateshiftdll.h"

namespace game{
	
	//method that performs the actual shuffle
	EXTERN_API uint8 _encrypt(uint8 value);

	//method that performs the actuall deshuffle
	EXTERN_API uint8 _decrypt(uint8 value);


	class AdvancedEncrypt final : public EncryptBase, public DecryptBase{

		//dispatch method which runs "times" times on call method
		//which is either _encrypt or _decrypt
		uint8 _shuffleNTimes(uint8 val, int times, decltype(_encrypt) call) const
		{
			if (times <= 1)	return call(val);
			else			return _shuffleNTimes(call(val), times-1, call);
		}
	public:
		void encrypt(EncType& message)
		{
			for(int i = 0; i < message.size(); ++i)
			{
				message[i] = _shuffleNTimes(message[i], 2, _encrypt);
				if (i%2)
				{
					message[i] += i%255;
				}
				else
				{
					message[i] -= i%255;
				}
			}
		}
		void decrypt(EncType& message)
		{
			for(int i = 0; i < message.size(); ++i)
			{
				if (i%2)
				{
					message[i] -= i%255;
				}
				else
				{
					message[i] += i%255;
				}
				message[i] = _shuffleNTimes(message[i], 2, _decrypt);
			}
		}
	};
}

#endif	//_ADVANCED_ALGORITHM_ENCRYPT_H_