/*

	rotateshift.h	-	made by Eduard Lahl

	Library that provides a simple class-based way to do
	circular shifts on integral variables.

	API: 

		class RotationShift
			
			template<class T>
			T left(T value, int shift)
				- performs circular left bit shift on value by shift

			template<class T>
			T right(T value, int shift)
				- performs circular right bit shift on value by shift

		instances:
			
			rotateShift
				- is guaranteed to be initialized when the program starts



*/

#ifndef _ROTATION_SHIFT_H_
#define _ROTATION_SHIFT_H_

namespace game{
	//simple class that performs circular shift on variable of type
	//T(template argument) and returns it
	class RotationShift final{
	public:
		template <class T>
		T left(T value, int shift)
		{
			return (value << shift) | (value >> (sizeof(value)*8 - shift));
		}
		template <class T>
		T right(T value, int shift)
		{
			return (value >> shift) | (value << (sizeof(value)*8 - shift));
		}
	}rotateShift = RotationShift();
};

#endif	//_ROTATION_SHIFT_H_