/*

	basicutils.h	-	made by Eduard Lahl

	This library is made for utilities that are used globally everywhere in the code.

	This file includes macros for debugging purposes, type casts from enum classes to
	unsigned ints for flags in various libraries.

	API: 

		#define DEBUG_FLAGS

		#define inherit virtual public

		typedefs of various ints: uint64, int64, uint32, uint, int32, uint16, int16, uint8, int8
								  note that all of those are inside of namespace game

		template<class T>
		unsigned int castEnum(T);

		template<class TO, class FROM>
		TO cowboycast(FROM);

*/

#ifndef _BASIC_UTILS_H_
#define _BASIC_UTILS_H_

//#define EXPORT_DLL

#ifdef EXPORT_DLL
#define EXTERN_API __declspec(dllexport)
#else
#define EXTERN_API __declspec(dllimport)
#endif

//says whether flags on various classes
//should be debugged for size protection
//for instance if we didnt pass value < 0
//or > maximum
//1 - does check
//0 - doesnt
#define DEBUG_FLAGS 1

//for virtual inheritance
//example usage:
//class C inherit A, cinherit B{ ... };
#define inherit  : public
#define cinherit public

namespace game{
//definitions of different tpyes of numerical values:
//the order is by size, and signifity:
typedef unsigned long long	uint64;
typedef long long			int64;

typedef unsigned int		uint32;
//alias for uint32
typedef uint32				uint;
//alias for int
typedef int					int32;

typedef unsigned short		uint16;
typedef short				int16;
typedef unsigned char		uint8;
typedef char				int8;
};

#include <memory>

//macro to use for mathematical operators expanding
//simple usage, example of class MyClass:
/*
	mathAPI(+, +=, float, SomeVar, MyClass)

	This expands to:

	MyClass& operator+(float t)	{ SomeVar += t; return *this; }
*/
#define mathAPI(OPERAND, OPERAND_2, ARGTYPE, VAR, THISTYPE) \
THISTYPE& operator##OPERAND(ARGTYPE t)	{ VAR ##OPERAND_2 t; return *this; }

//when you have class and want to use all mathematical binary
//operations such as a + b, a+= b ...
//example usage:
/*
	class MyClass{
		mathAPIFull(float, SomeVar, MyClass)
	};
	
	expands into:

	class MyClass{
		MyClass& operator+(float t)		{ SomeVar += t; return *this; }
		MyClass& operator+=(float t)	{ SomeVar += t; return *this; }
		//and so on
	};
*/
#define mathAPIFull(ARGTYPE, VAR, THISTYPE) \
	THISTYPE& operator+(ARGTYPE t)	{ VAR += t; return *this; }\
	THISTYPE& operator+=(ARGTYPE t)	{ VAR += t; return *this; }\
	THISTYPE& operator-(ARGTYPE t)	{ VAR -= t; return *this; }\
	THISTYPE& operator-=(ARGTYPE t)	{ VAR -= t; return *this; }\
	THISTYPE& operator*(ARGTYPE t)	{ VAR *= t; return *this; }\
	THISTYPE& operator*=(ARGTYPE t)	{ VAR *= t; return *this; }\
	THISTYPE& operator/(ARGTYPE t)	{ VAR /= t; return *this; }\
	THISTYPE& operator/=(ARGTYPE t)	{ VAR /= t; return *this; }


template <class T>
//helper function for easier and nontemplate declared
//typing of unique_ptr<T>
std::unique_ptr<T> make_unique(T* val)
{
	return std::unique_ptr<T>(val);
}

template <class T>
//helper function to statically convert integral values
//to enums
//keep in mind that this, unlike castEnum requires
//explicitly specified template type, because
//the compiler cannot deduce it by return type
T castToEnum(game::uint val)
{
	return static_cast<T>(val);
}

template <class T>
//helper function to statically convert values from
//enums marked as enum class to unsigned ints
inline unsigned int castEnum(T val)
{
	return static_cast<unsigned int>(val);
}

//use if you want to have bit or, and and xor operations
//on enum classes with purpose of being flags
//example usage:
//
//enum class MyEnum{ ... };
//enumExpandOperators(MyEnum)
#define enumExpandOperators(ENUMTYPE)\
	game::uint operator|(ENUMTYPE a, ENUMTYPE b){return (castEnum(a) | castEnum(b));}\
	game::uint operator&(ENUMTYPE a, ENUMTYPE b){return (castEnum(a) & castEnum(b));}\
	game::uint operator^(ENUMTYPE a, ENUMTYPE b){return (castEnum(a) ^ castEnum(b));}\
	ENUMTYPE operator|(game::uint a, ENUMTYPE b){return castToEnum<ENUMTYPE>(a | castEnum(b));}\
	ENUMTYPE operator&(game::uint a, ENUMTYPE b){return castToEnum<ENUMTYPE>(a & castEnum(b));}\
	ENUMTYPE operator^(game::uint a, ENUMTYPE b){return castToEnum<ENUMTYPE>(a ^ castEnum(b));}


template <class A, class B>
//fast reinterpret_cast made with union.
A cowboyCast(B val)
{
	union{
		A v;
		B q;
	}a;
	a.q = val;
	return a.v;
}

#define DCOUT	std::cout << __LINE__<< "\n";

#endif	//_BASIC_UTILS_H_