
#ifndef _UTILS_H_
#define _UTILS_H_

#include <string>
#include <vector>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

template <class T>
std::ostream& operator<<(std::ostream& stream, const sf::Vector2<T>& vec)
{
	return stream << "x: " << vec.x << " y: " << vec.y;
}

sf::Vector2u operator*(const sf::Vector2u& lhs, float rhs)
{
	return sf::Vector2u(lhs.x*rhs, lhs.y*rhs);
}

template <class T>
sf::Vector2u operator*(const sf::Vector2u& lhs, sf::Vector2<T> rhs)
{
	return sf::Vector2u(lhs.x*rhs.x, lhs.y*rhs,y);
}

template <class Callable>
/*
	errorCallBack requires 1 argument of type const std::string&
*/
void loadEncryptedFile(const std::string& filePath, sf::Texture& texture, Callable& errorCallBack)
{
	static_assert(!std::is_function<Callable>::value, "Callable(errorCallBack) must be callable object");
	std::string memory;
	game::EncryptedFileC file(filePath);
	file.read(memory, file.getSize());
	if (!texture.loadFromMemory(&memory[0], memory.size()))
		errorCallBack(filePath);
}

inline void loadEncryptedFile(const std::string& filePath, sf::Texture& texture)
{
	return loadEncryptedFile(filePath, texture,
							[](const std::string& path){ std::cout << "error loading " << path << "\n";});
}

int convert(const std::string& str)
{
	if (!str.size())
		return 0;

	int v = 0;
	std::string copy = str;
	bool isNeg = str[0] == '-';
	int strt = 0;
	if (isNeg) strt++;
	for(int i = strt, j = copy.size(); i < j; ++i)
	{
		v *= 10;
		v += (copy.at(i) - '0');
	}
	return isNeg ? -v : v;
}

long long convert64(const std::string& str)
{
	long long v = 0;
	std::string copy = str;
	bool isNeg = str[0] == '-';
	int strt = 0;
	if (isNeg) strt++;
	for(int i = strt, j = copy.size(); i < j; ++i)
	{
		v *= 10;
		v += (copy.at(i) - '0');
	}
	return isNeg ? -v : v;
}

void reverseString(std::string& str)
{
	std::string a = str;
	str.clear();
	str.insert(str.begin(), a.rbegin(), a.rend());
}

std::string format(long long arg)
{
	std::string s;
	int digits = (std::log10(arg)+1);
	int firstcomma = digits%3;
	int commaCopy = firstcomma;
	if (!arg)
		return "0";

	if (digits < 4)
	{
		for(int i = 0; i < digits; ++i)
		{
			s.push_back(arg%10 + '0');
			arg /= 10;
		}
		reverseString(s);
		return s;
	}

	for(int i = 0, j = digits - firstcomma; i < j; ++i)
	{
		if (!(i%3) && i)
		{
			s.push_back(',');
		}
		s.push_back(arg%10 + '0');
		arg /= 10;
	}
	if ((digits%3))
		s.push_back(',');

	while(commaCopy)
	{
		s.push_back(arg%10 + '0');
		arg /= 10;
		--commaCopy;
	}
	reverseString(s);
	return s;
}

std::string index2String(int index)
{
	std::string names[] = {
		"Total Level", "Attack", "Defence",
		"Strength", "Constitution", "Ranged",
		"Prayer", "Magic", "Cooking",
		"Woodcutting", "Fletching", "Fishing",
		"Firemaking", "Crafting", "Smithing",
		"Mining", "Herblore", "Agility",
		"Thieving", "Slayer", "Farming",
		"Runecrafting", "Hunter", "Construction",
		"Summoning", "Dungeoneering", "Divination"
	};
	return (index < 0 || index >= 27) ? "" : names[index];
}

int findIndexNames(const std::string& name)
{
	std::string names[] = {
		"Total Level", "Attack", "Defence",
		"Strength", "Constitution", "Ranged",
		"Prayer", "Magic", "Cooking",
		"Woodcutting", "Fletching", "Fishing",
		"Firemaking", "Crafting", "Smithing",
		"Mining", "Herblore", "Agility",
		"Thieving", "Slayer", "Farming",
		"Runecrafting", "Hunter", "Construction",
		"Summoning", "Dungeoneering", "Divination"
	};
	return (std::find(begin(names), end(names), name) - names);

}

template <class T>
void insert(std::vector<T>& vec, int value)
{
	vec.push_back(value);
}

void loadXps(std::vector<int>& xps)
{
	insert(xps, 0); insert(xps, 83); insert(xps, 174); insert(xps, 276);
	insert(xps, 388); insert(xps, 512); insert(xps, 650); insert(xps, 801);
	insert(xps, 969); insert(xps, 1154); insert(xps, 1358); insert(xps, 1584);
	insert(xps, 1833); insert(xps, 2107); insert(xps, 2411); insert(xps, 2746);
	insert(xps, 3115); insert(xps, 3523); insert(xps, 3973); insert(xps, 4470);
	insert(xps, 5018); insert(xps, 5624); insert(xps, 6291); insert(xps, 7842);
	insert(xps, 8740); insert(xps, 9730); insert(xps, 7028); insert(xps, 10824);
	insert(xps, 12031); insert(xps, 13363); insert(xps, 14833); insert(xps, 16456);
	insert(xps, 18247); insert(xps, 20224); insert(xps, 22406); insert(xps, 24815);
	insert(xps, 27473); insert(xps, 30408); insert(xps, 33648); insert(xps, 37224);
	insert(xps, 41171); insert(xps, 45529); insert(xps, 50339); insert(xps, 55649);
	insert(xps, 61512); insert(xps, 67983); insert(xps, 75124); insert(xps, 83014);
	insert(xps, 91721); insert(xps, 101333); insert(xps, 111945); insert(xps, 123660);
	insert(xps, 136594); insert(xps, 150872); insert(xps, 166636); insert(xps, 184040);
	insert(xps, 203254); insert(xps, 224466); insert(xps, 247886); insert(xps, 273742);
	insert(xps, 302288); insert(xps, 333804); insert(xps, 368599); insert(xps, 407015);
	insert(xps, 449428); insert(xps, 496254); insert(xps, 547953); insert(xps, 605032);
	insert(xps, 668051); insert(xps, 747627); insert(xps, 814445); insert(xps, 899257);
	insert(xps, 992895); insert(xps, 1096278); insert(xps, 1210421); insert(xps, 1336443);
	insert(xps, 1475581); insert(xps, 1629200); insert(xps, 1798808); insert(xps, 1986068);
	insert(xps, 2192818); insert(xps, 2421087); insert(xps, 2673114); insert(xps, 2951373);
	insert(xps, 3258594); insert(xps, 3597792); insert(xps, 3972294); insert(xps, 4385776);
	insert(xps, 4842295); insert(xps, 5346332); insert(xps, 5902831); insert(xps, 6517253);
	insert(xps, 7195629); insert(xps, 7944614); insert(xps, 8771588); insert(xps, 9684577);
	insert(xps, 10692629); insert(xps, 11805606); insert(xps, 13034431); insert(xps, 14391160);
	insert(xps, 15889109); insert(xps, 17542976); insert(xps, 19368992); insert(xps, 21385073);
	insert(xps, 23611006); insert(xps, 26068632); insert(xps, 28782069); insert(xps, 31777943);
	insert(xps, 35085654); insert(xps, 38737661); insert(xps, 42769801); insert(xps, 47221641);
	insert(xps, 52136869); insert(xps, 57563718); insert(xps, 63555443); insert(xps, 70170840);
	insert(xps, 77474828); insert(xps, 85539082); insert(xps, 94442737); insert(xps, 104273167);
}

sf::IntRect copyRectMove(const sf::IntRect& source, int moveX, int moveY)
{
	sf::IntRect newR = source;
	newR.top += moveX;
	newR.height += moveY;
	return newR;
}

void loadCoords(std::vector<sf::IntRect>& vec)
{
	const auto width = 93-10;
	const auto height = 40-5;

	const auto widthRight = 76;
	/*
		5
		38
		71
		104
		138
		171
		204
		237
		271
	*/
	vec.push_back(sf::IntRect(80, 334, (195-80), (373-334)));	//ttl
	vec.push_back(sf::IntRect(5, 5, width, height));			//att
	vec.push_back(sf::IntRect(5, 71, width, height));			//def
	vec.push_back(sf::IntRect(5, 38, width, height));			//str
	vec.push_back(sf::IntRect(88, 5, width, height));			//hp
	vec.push_back(sf::IntRect(5, 104, width, height));			//rng
	vec.push_back(sf::IntRect(5, 137, width, height));			//pray
	vec.push_back(sf::IntRect(5, 171, width, height));			//magic
	vec.push_back(sf::IntRect(171, 104, widthRight, height));	//cook
	vec.push_back(sf::IntRect(171, 171, widthRight, height));	//wc
	vec.push_back(sf::IntRect(88, 171, width, height));			//fletch
	vec.push_back(sf::IntRect(171, 71, widthRight, height));	//fish
	vec.push_back(sf::IntRect(171, 137, widthRight, height));	//fm
	vec.push_back(sf::IntRect(88, 137, width, height));			//craft
	vec.push_back(sf::IntRect(171, 38, widthRight, height));	//smith
	vec.push_back(sf::IntRect(171, 5, widthRight, height));		//mining
	vec.push_back(sf::IntRect(88, 71, width, height));			//herb
	vec.push_back(sf::IntRect(88, 38, width, height));			//agil
	vec.push_back(sf::IntRect(88, 104, width, height));			//thief
	vec.push_back(sf::IntRect(88, 204, width, height));			//slay
	vec.push_back(sf::IntRect(171, 204, widthRight, height));	//farm
	vec.push_back(sf::IntRect(5, 204, width, height));			//rc
	vec.push_back(sf::IntRect(88, 237, width, height));			//hunt
	vec.push_back(sf::IntRect(5, 237, width, height));			//const
	vec.push_back(sf::IntRect(171, 237, widthRight, height));	//summ
	vec.push_back(sf::IntRect(5, 271, width, height));			//dung
	vec.push_back(sf::IntRect(88, 271, width, height));			//div
}

void loadSkillIcons(std::vector<sf::Texture>& vec, std::vector<sf::Sprite>& spVec)
{
	game::EncryptedFileC enFile;
	std::string s;
	vec.clear();
	vec.resize(27);

	spVec.clear();
	spVec.resize(27);

	for(int i = 0; i < 27; ++i)
	{
		enFile.close();
		std::string path = "pictures/";
		std::string buffer;
		path.append(index2String(i).append("Icon.pngcyp"));
		enFile.open(path);
		enFile.read(buffer, enFile.getSize());
		auto& tex = vec.at(i);
		tex.loadFromMemory(&buffer[0], buffer.size());
		spVec.at(i).setTexture(tex);
	}
}

void scaleSprite(int pixelsX, int pixelsY, sf::Sprite& sprite)
{
	auto tex = sprite.getTexture();
	if (!tex)	return;
	auto texSize = tex->getSize();
	auto newTexSize = sf::Vector2u(pixelsX, pixelsY);
	auto scaleX = newTexSize.x*1.00f/texSize.x;
	auto scaleY = newTexSize.y*1.00f/texSize.y;
	sprite.scale(scaleX, scaleY);
}

#endif	//_UTILS_H_