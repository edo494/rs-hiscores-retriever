/*

	filemanipc.h	-	made by Eduard Lahl

	This header contains class FileManipC, which does basic modifications
	on files.
	Allows writing to files, reading from files, renaming files, removing them
	from hard disk as well as remove content from file.
	This class manipualtes C object FILE*.

	API: 

		//private, nonusable
		FileManipC(const FileManipC&);
		FileManipC& operator=(const FileManipC&);
			- unusable, because fstream itself cannot be copy constructed or copy assigned.


		FileManipC()
			- defualt constructor

		FileManipC(std::string&& fileName)
			- move construct

		FileManipC(const std::string& fileName)
			- construct with file path specified

		~FileManipC()
			- destructor
			- upon calling, the FileManipC class closes associtated file

		void open()
			- re-open the file.
			- if filePath == "", does nothing

		void open(std::string& file_path)
			- opens file at given path
			- call to this function modifies filePath
		
		int64 read(std::string& buffer, uint count)
			- reads "count" bytes from file, storing them inside "buffer" object
			- if the file was not opened correctly or the reading failed, returns 0, otherwise returns
			  numbers of characters read
			- this function call has safety check if the container is big enough
			  if it isnt, the container is grown to enough size to store "count" bytes
		
		int64 read(void* buffer, uint count)
			- reads "count" bytes from file, storing them inside "buffer" object
			- if the file was not opened correctly or the reading failed, returns 0, otherwise returns
			  numbers of characters read
			- this function call doesnt have any safety checkes regarding size of container
			- if the container is smaller than "count", the function only stores "size" bytes in container
			
		int64 peek(std::string& buffer, uint count)
			- reads "count" bytes from file, storing them inside "buffer" object.
			- if the file was not opened correctly or the reading failed, returns 0, otherwise returns
			  numbers of characters read
			- this function doesnt move the position from which we are reading from
			  or which we are writing to
			  
		int64 peek(void* buffer, uint count)
			- reads "count" bytes from file, storing them inside "buffer" object.
			- if the file was not opened correctly or the reading failed, returns 0, otherwise returns
			  numbers of characters read
			- this function doesnt move the position from which we are reading from
			  or which we are writing to
			  
		int64 readAll(std::string& buffer)
			- reads all content stored inside file into buffer
			- if buffer is not big enough, this function automatically resizes it
			- if reading fails, returns 0, otherwise returns numbers of characters read

		int64 readAll(void* buffer)
			- reads all content stored inside file into buffer
			- if buffer is not big enough, this function automatically resizes it
			- if reading fails, returns false

		bool write(std::string& buffer, uint pos = -1)
			- write content from "buffer" into file at position "pos"
			- if position is not specified, or is set to -1, the position is current position.
			- otherwise the write happens at given position
			- if file was not opened successfuly or the writing was unsuccessful, returns false.

		void remove(uint32 position, uint howMany)
			- removes "howMany" bytes from file at "position" position
			- this call is very slow, because it has to recreate the file
			- if howMany + position > size of file, removes content from position until end of file

		uint size()
			- return size of the file

		uint left()
			- return number of bytes left in the file until the end of file

		uint current()
			- return current position of the stream

		void flushString()
			- reset filePath string, invalidating calls to open()

		void removeFile()
			- remove file from hard disk

		void recreateFile()
			- recreate the file
			- this is the same as calling remove(0, -1);

		void renameFile(std::string& newName, bool overWrite = false)
			- rename the file
			- call to this function also changes filePath variable, if file was renamed
			  successfuly
			- if overWrite is set to true and the file already exists, the file gets overwriten
			- if overWrite is false and the file already exists, nothing happens

		void close()
			- closes the file
			- this does not reset filePath

		inline bool atEnd()
			- returns whether the file is at its end(the pointer we are writing to/reading from)

		void move(uint absolutepos, uint flag)
			- moves the reading/writing pointer to "absolutepos" with flag "flag"
			- allowed flags:		std::ios::beg
								std::ios::cur
								std::ios::end

		void move(uint absolutepos)
			- moves the reading/writing pointer to "absolutepos" from the beginning of the file
			- this has the same effect as calling move(absolutepos, std::ios::beg);

		void moveRelative(uint relative, bool trueForward)
			- moves the writing/reading pointer relativelly to the current pointer
			- if trueForward is true, moves forward(towards the end of the file)
			  otherwise moves backwards(towards the beginning of the file)

		bool isOpen()
			- returns whether the file is open or not

*/
#ifndef _FILE_MANIP_C_H_
#define _FILE_MANIP_C_H_

#include "basicutilsdll.h"
#include <cstdio>
#include <string>


namespace game{
	class FileManipC{
	protected:
		std::FILE* file;
		uint fileSize;
		std::string filePath;
		
		void _initFile()
		{
			if (file)
				std::fclose(file);

			file = std::fopen(filePath.c_str(), "r+b");
			if (!file)
			{
				file = std::fopen(filePath.c_str(), "w");
				return _initFile();
			}
			std::fseek(file, 0, SEEK_END);
			fileSize = std::ftell(file);
			std::rewind(file);
		}

		//private, nonusable
		FileManipC(const FileManipC&);
		FileManipC& operator=(const FileManipC&);
	public:
		//default construct
		FileManipC() : filePath()
		{
			file = nullptr;
		}

		//move construct
		FileManipC(std::string&& fileName) : filePath(std::move(fileName))
		{
			_initFile();
		}

		//construct
		FileManipC(const std::string& fileName) : filePath(fileName)
		{
			_initFile();
		}

		//re-opens the file
		//if filePath == "", does nothing
		bool open()
		{
			if (filePath != "")
			{
				_initFile();
				return true;
			}
			return false;
		}

		//opens file at given path
		bool open(const std::string& file_path)
		{
			if (filePath != file_path)
			{
				filePath = file_path;
				_initFile();
				return true;
			}
			return false;
		}
		
		//reads count data from buffer storing it inside buffer
		//if file was not opened successfuly, returns.
		//if can not read from file, returns 0, otherwise returns number of characters read
		int64 read(std::string& buffer, uint count)
		{
			if (!isOpen())	return 0;
			if (buffer.size() < count)	buffer.resize(count, ' ');
			std::fflush(file);
			return std::fread(&buffer[0], sizeof(buffer[0]), count, file);
		}
		
		//reads count data from buffer storing it inside buffer
		//if file was not opened successfuly, returns.
		//if can not read from file, returns 0, otherwise returns number of characters read
		int64 read(void* buffer, uint count)
		{
			if (!isOpen())	return 0;
			std::fflush(file);
			return std::fread(buffer, sizeof(char), count, file);
		}

		//peek count data from buffer, keeping it in place and storing the
		//read data inside buffer.
		//if file was not opened successfuly, returns 0.
		//if can not read from file, returns 0, otherwise returns number of characters read
		int64 peek(std::string& buffer, uint count)
		{
			uint64 cPos = std::ftell(file);
			auto a = read(buffer, count);
			std::fseek(file, cPos, SEEK_SET);
			return a;
		}
		
		//peek count data from buffer, keeping it in place and storing the
		//read data inside buffer.
		//if file was not opened successfuly, returns 0.
		//if can not read from file, returns 0, otherwise returns number of characters read
		int64 peek(void* buffer, uint count)
		{
			uint64 cPos = std::ftell(file);
			auto a = read(buffer, count);
			std::fseek(file, cPos, SEEK_SET);
			return a;
		}

		//read everything from the file, from the beginning to the end
		//this does not move the position we are writing to/reading from.
		//if can not read from file, returns 0, otherwise returns number of characters read
		int64 readAll(std::string& buffer)
		{
			if (!isOpen())	return 0;
			uint64 pos = std::ftell(file);
			std::fseek(file, 0, SEEK_SET);
			if (buffer.size() < fileSize)	buffer.resize(fileSize, ' ');
			std::fflush(file);
			auto a = std::fread(&buffer[0], sizeof(buffer[0]), fileSize, file);
			std::fseek(file, pos, SEEK_SET);
			return a;
		}
		
		//read everything from the file, from the beginning to the end
		//this does not move the position we are writing to/reading from.
		//if can not read from file, returns 0, otherwise returns number of characters read
		int64 readAll(void* buffer)
		{
			if (!isOpen())	return 0;
			uint64 pos = std::ftell(file);
			std::fflush(file);
			return std::fread(buffer, sizeof(char), fileSize, file);
		}
		
		//writes content from buffer into file at pos(defaulted to end of file)
		//if can not write to the file, returns false, otherwise returns true
		bool write(std::string& buffer, uint pos = -1)
		{
			if (!isOpen())	return false;
			if (pos != -1)
				std::fseek(file, pos, SEEK_SET);
			else
				std::fseek(file, std::ftell(file), SEEK_SET);

			uint fS = fileSize;
			std::fflush(file);
			return (fileSize += std::fwrite(&buffer[0], sizeof(buffer[0]), buffer.size(), file) > fS);
		}

		//removes content from file.
		//Very slow operation.
		//if file was not opened successfuly, returns.
		void remove(uint32 position, uint howMany)
		{
			if (!isOpen())	return;
			std::string s;
			s.resize(fileSize, ' ');
			std::fflush(file);
			std::fread(&s[0], sizeof(s[0]), fileSize, file);
			close();
			if (howMany + position <= fileSize)
				s.erase(position, howMany);
			else
				s.erase(position);

			file = std::fopen(filePath.c_str(), "w");
			std::fwrite(&s[0], sizeof(s[0]), s.size(), file);
			close();
			open();
			fileSize -= howMany;
		}

		//return size of the file
		uint size()
		{
			return fileSize;
		}

		//returns number of bytes left in the file before end of file
		uint left()
		{
			return static_cast<uint>(fileSize - std::ftell(file));
		}

		//return current position of the stream
		uint current()
		{
			return std::ftell(file);
		}

		//flushes the string stored which holds the path to the file
		void flushString()
		{
			filePath.swap(std::string(""));
		}

		//removes the file from hard disk
		//does not reset filePath
		void removeFile()
		{
			close();
			std::remove(filePath.c_str());
		}

		//recreates the file, making it empty
		void recreateFile()
		{
			close();
			file = std::fopen(filePath.c_str(), "wb");
			_initFile();
		}

		//renames the file
		//this changes the filePath variable
		//if overWrite is set, and file with name of newName already exists
		//the file gets overwriten
		void renameFile(std::string& newName, bool overWrite = false)
		{
			close();
			if (overWrite)
			{
				FileManipC f(newName);
				f.removeFile();
			}
			::rename(filePath.c_str(), newName.c_str());
			filePath = newName;
			_initFile();
		}

		//closes the file
		//does not reset filePath
		void close()
		{
			if (file)
				std::fclose(file);

			file = nullptr;
		}

		//returns whether the file is at its end or not
		inline bool atEnd()
		{
			return (std::feof(file) == EOF);
		}

		//move the position we are reading from/writing to
		//allowed flags:	SEEK_SET
		//					SEEK_CUR
		//					SEEK_END
		void move(uint absolutepos, uint flag)
		{
			#if _DEBUG
				if (flag != SEEK_SET || flag != SEEK_CUR || flag != SEEK_END)	return;
			#endif
			std::fseek(file, absolutepos, flag);
		}

		//moves the position we are reading from/writing to
		//absolutly from the beginning of the file to absolutepos
		void move(uint absolutepos)
		{
			std::fseek(file, absolutepos, SEEK_SET);
		}

		//moves the position we are reading from/writing to
		//relativelly to current position
		//if trueForward == true, moves forward, if trueForward == false, moves backwards
		void moveRelative(uint relative, bool trueForward)
		{
			std::fseek(file, trueForward ? std::ftell(file) + relative : std::ftell(file), SEEK_SET);
		}

		//returns whether the file is opened or not
		bool isOpen()
		{
			return (file != nullptr);
		}
		
		//destructing the object
		virtual ~FileManipC()
		{
			close();
		}
	};
};

#endif	//_FILE_MANIP_C_H_