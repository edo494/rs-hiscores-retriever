/*

	encryption.h	-	made by Eduard Lahl

	This Library provides interface classes for Encryption and Decryption of
	strings(std::string objects) to inherit from as well as basic Decryption
	and Encryption(no encryption/decryption).

	Pointers to those interface classes are used in other libraries of this project,
	the default types are always BasicEncryption and BasicDecryption, if you want
	to pass your own, inherit them(preffered virtually).

	API: 
		
		class EncryptBase;

		class DecryptBase;

		class BasicEncrypt : EncryptBase;

		class BasicDecrypt : DecryptBase;

	Detailed API:

		class EncryptBase
			
			typedefs:	EncType - std::string
							- typedef to the object type we want to encrypt
							
			methods:	virtual EncType encrypt(const EncType&) = 0;
							- method that performs the encryption

						virtual ~EncryptBase();
							- in case of custom destructor that needs to destroy
							  runtime allocated objects


		class DecryptBase	
			
			typedefs:	DecType - std::string
							- typedef to the object type we want to decrypt

			methods:	virtual DecType decrypt(const DecType&) = 0;
							- method that performs the decryption

						virtual ~DecryptBase()
							- in case of custom destructor that needs to destroy
							  runtime allocated objects

		
		class BasicEncrypt : public virtual EncryptType
			
			typedefs:	EncType - std::string

			methods:	EncType encrypt(const EncType& message)
							- returns the message as is


		class BasicDecrypt : public virtual DecryptType
			
			typedefs:	DecType - std::string

			- methods:	DecType decrypt(const DecType& message)
							- returns the message as is

*/

#ifndef _ENCRYPTION_H_
#define _ENCRYPTION_H_

#include "basicutilsdll.h"
#include <string>

namespace game{
	//interface class to use when encrypting data
	class EncryptBase{
	public:
		typedef std::string EncType;
		virtual void encrypt(EncType&) = 0;
		virtual ~EncryptBase() {}
	};

	//basic decryption class that does no
	//decryption at all, this is default class type
	//in places where DecryptBase is expected
	class BasicEncrypt inherit EncryptBase{
	public:
		void encrypt(EncType& message)
		{
		}
	};

	//interface class to use when decrypting data
	class DecryptBase{
	public:
		typedef std::string DecType;
		virtual void decrypt(DecType&) = 0;
		virtual ~DecryptBase() {}
	};
		
	//basic encryption class that does no
	//encryption at all, this is default class type
	//in places where EncryptBase is expected
	class BasicDecrypt inherit DecryptBase{
	public:
		void decrypt(DecType& message)
		{
		}
	};
}

#endif	//_ENCRYPTION_H_