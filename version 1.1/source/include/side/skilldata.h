
#ifndef _SKILL_DATA_H_
#define _SKILL_DATA_H_

#include <string>

struct SkillData final{
	long long rank;
	long long lvl;
	long long xps;
	std::string name;
};

#endif	//_SKILL_DATA_H_