/*

	encryptedfilec.h	-	made by Eduard Lahl

	This library provides class and methods compatable with SFML(inheriting sf::InputStream)
	to read and write to file using AdvAlg encryption using C's FILE*

	API:

		class EncryptedFileC : sf::InputStream;

*/

#ifndef _ENCRYPTED_FILE_C_H_
#define _ENCRYPTED_FILE_C_H_

#include "filemanipcdll.h"
#include "advalgdll.h"
#include <SFML/System/InputStream.hpp>

namespace game{
	class EncryptedFileC : public sf::InputStream{
		FileManipC file;
	public:
		EncryptedFileC() : file() {}
		
		EncryptedFileC(const std::string& name) : file(name) {}

		inline void close()
		{
			file.close();
		}

		inline void open(const std::string& name)
		{
			file.close();
			file.open(name);
		}

		sf::Int64 read(void* data, sf::Int64 size)
		{
			auto diff = file.read(data, size);
			if (!diff) return -1;
			auto c = static_cast<char*>(data);
			std::string s(c, diff);
			AdvancedEncrypt().decrypt(s);
			return diff;
		}

		uint read(std::string& buffer, uint size)
		{
			auto diff = file.read(buffer, size);
			if (!diff)	return -1;
			AdvancedEncrypt().decrypt(buffer);
			return diff;
		}

		void write(std::string& buffer, uint pos = -1)
		{
			std::string s = buffer;
			AdvancedEncrypt().encrypt(s);
			file.write(s, pos);
		}

		sf::Int64 seek(sf::Int64 pos)
		{
			auto a = file.current();
			file.move(pos);
			return file.current() - a;
		}

		sf::Int64 tell()
		{
			return file.current();
		}

		sf::Int64 getSize()
		{
			return file.size();
		}
	};
}

#endif	//_ENCRYPTED_FILE_C_H_