
#ifndef _CONF_READER_H_
#define _CONF_READER_H_

#include <string>
#include "filemanipc.h"
#include "stringiterator.h"

class ConfigReader{
	game::FileManipC file;
	std::string content;
	bool isDelim(char c)
	{
		return (c == '\n' || c == '\t' || c == ' ' || c == '=');
	}
public:
	explicit ConfigReader(const std::string& name) : file(name)
	{
		file.open();
		file.readAll(content);
		file.close();
	}
	std::string read(const std::string& field)
	{
		std::string s = content;
		if (s.size() < field.size())	return "";
		int aSize = field.size() - 1;
		for(int i = 0; i < s.size() - aSize; ++i)
		{
			std::string inner = s.substr(i, aSize+1);
			if (inner == field)
			{
				int first = i + aSize+1;
				while(first < s.size() && isDelim(s[first]))	++first;

				int second = first;
				while(second < s.size() && !isDelim(s[second]))	++second;

				return s.substr(first, second - first);
			}
		}
		return "";
	}

	void update()
	{
		file.open();
		file.readAll(content);
		file.close();
	}
};

#endif	//_CONF_READER_H_