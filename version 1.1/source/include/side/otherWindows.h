
#ifndef _OTHER_WINDOWS_H_
#define _OTHER_WINDOWS_H_

#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <string>
#include <vector>
#include <atomic>

#include "utils.h"

enum showOption{
	sortXpsRemaining,
	sortXps,
	sortRank,
	sortXps200
};

void showWindowOrdered(std::vector<SkillData>& vec, showOption option, sf::RenderWindow& parent, sf::VideoMode vMode,\
													const std::string& name, sf::Uint8 mode,\
													int panelH)
{
	sf::Font font;
	font.loadFromFile("fonts/runescape_chat_font.ttf");
	
	game::EncryptedFileC enFile;

	std::vector<sf::Texture> texVec;

	for(int i = 0; i < 27; ++i)
	{
		enFile.close();
		std::string path = "pictures/";
		std::string buffer;
		path.append(index2String(i).append("Icon.pngcyp"));
		enFile.open(path);
		enFile.read(buffer, enFile.getSize());
		sf::Texture tex;
		tex.loadFromMemory(&buffer[0], buffer.size());
		texVec.push_back(tex);
	}
	enFile.close();
	
	std::vector<sf::Sprite> sprVec;

	std::vector<sf::Text> textVec;

	for(int i = 0; i < texVec.size(); ++i)
	{
		sf::Sprite spr;
		spr.setTexture(texVec[i]);
		scaleSprite(28, 28, spr);
		sprVec.push_back(spr);
		sf::Text text;
		text.setFont(font);
		text.setColor(sf::Color::Yellow);
		text.setCharacterSize(25);
		text.setPosition(35, i*28);
		textVec.push_back(text);
	}

	std::vector<sf::Text> textValVec;
	for(int i = 0; i < textVec.size(); ++i)
	{
		sf::Text text;
		text.setFont(font);
		text.setColor(sf::Color::Yellow);
		text.setCharacterSize(25);
		textValVec.push_back(text);
	}

	std::vector<SkillData> sorted;
	sorted.insert(sorted.begin(), vec.begin(), vec.end());
	for(auto&& a : sorted)
	{
		if (a.xps == -1)
		{
			a.lvl = 1;
			a.xps = 0;
			a.rank = 2000000;
		}
	}

	int offset = 0;

	if (option == showOption::sortXps)
	{
		offset = 350;
		std::sort(sorted.begin(), sorted.end(), [](const SkillData& a, const SkillData& b){
			return a.xps > b.xps;
		});
	}
	else if (option == showOption::sortRank)
	{
		offset = 285;
		std::sort(sorted.begin(), sorted.end(), [](const SkillData& a, const SkillData& b){
			return a.rank < b.rank;
		});
	}
	else if (option == showOption::sortXpsRemaining)
	{
		offset = 385;
		const auto xps99 = 13034431;
		const auto xps120 = 104273167;
		for(int i = 0; i < sorted.size(); ++i)
		{
			int left = 0;
			if (sorted[i].name == "Dungeoneering")
			{
				if (sorted[i].lvl == -1)
				{
					sorted[i].xps = 0;
					sorted[i].lvl = 1;
				}
				else if (sorted[i].lvl < 120)
				{
					left = xps120 - sorted[i].xps;
					sorted[i].xps = left;
				}
				else
				{
					sorted[i].xps = 0;
				}
			}
			else
			{
				if (sorted[i].lvl < 99)
				{
					left = xps99 - sorted[i].xps;
					sorted[i].xps = left;
				}
				else
				{
					sorted[i].xps = 0;
				}
			}
		}

		std::sort(sorted.begin(), sorted.end(), [](const SkillData& a, const SkillData& b){
			return a.xps > b.xps;
		});
	}
	else if (option == showOption::sortXps200)
	{
		offset = 400;
		for(auto&& a : sorted)
		{
			if (!(a.name == "Total Level"))
				a.xps = 2e8 - a.xps;
			else
				a.xps = 0;
		}

		std::sort(sorted.begin(), sorted.end(), [](const SkillData& a, const SkillData& b){
			return a.xps > b.xps;
		});
	}

	for(int i = 0; i < textVec.size(); ++i)
	{
		textVec[i].setString(sorted[i].name + ":");
		if (option == showOption::sortRank)
			textValVec[i].setString(format(sorted[i].rank));
		else
			textValVec[i].setString(format(sorted[i].xps) + " Xps");

		if (option == showOption::sortXpsRemaining || option == showOption::sortXps200)
			textValVec[i].setString(textValVec[i].getString() + " left");

		auto bounds = textValVec[i].getGlobalBounds();
		textValVec[i].setPosition((offset - bounds.width), (i)*28 + 1);
		if (i%2)
		{
			textVec[i].setColor(sf::Color::White);
			textValVec[i].setColor(sf::Color::White);
		}
	}

	for(int i = 0; i < sorted.size(); ++i)
	{
		auto name = sorted[i].name;
		auto index = findIndexNames(name);
		sprVec[index].setPosition(1, i*28);
	}

	std::string s;
	if (option == showOption::sortRank)
	{
		s = " Rank";
	}
	else if (option == showOption::sortXps)
	{
		s = " Xps";
	}
	else if (option == showOption::sortXpsRemaining)
	{
		s = " Remaining Xps";
	}
	else if (option == showOption::sortXps200)
	{
		s = " Xps until 200m";
	}
	
	sf::Text goBack;
	goBack.setFont(font);
	goBack.setColor(sf::Color::Yellow);
	goBack.setString("Go Back");
	auto goBackBounds = goBack.getGlobalBounds();
	goBack.setPosition((offset+5 - goBackBounds.width)/2, 760);

	parent.create(sf::VideoMode(offset+5, 800), "Sorted by" + s, sf::Style::Close | sf::Style::Titlebar);

	auto dMode = sf::VideoMode::getDesktopMode();

	if (dMode.height - panelH < parent.getSize().y)
	{
		auto newY = 400;
		auto forEach = newY/15;

		auto pivot = 14;

		parent.create(sf::VideoMode(2*(offset+5) + 10, 400), "Sorted by " + s, sf::Style::Close | sf::Style::Titlebar);

		for(int i = 0; i < texVec.size(); ++i)
		{
			sf::Sprite spr = sprVec[i];
			spr.setScale(1.0f, 1.0f);
			scaleSprite(forEach, forEach, spr);
		}

		int stepper = forEach;

		auto textBounds = textVec[0].getGlobalBounds();
		auto textSize = textVec[0].getCharacterSize();

		while(textBounds.height > forEach)
			textVec[0].setCharacterSize(--textSize);

		for(int i = 0; i < pivot; ++i)
		{
			auto name = sorted[i].name;
			auto index = findIndexNames(name);
			sprVec[index].setPosition(1, i*stepper);
		}
		
		for(int i = pivot; i < sprVec.size(); ++i)
		{
			auto name = sorted[i].name;
			auto index = findIndexNames(name);
			sprVec[index].setPosition((2*(offset+5) + 10)/2, (i-pivot)*stepper);
		}

		for(int i = 0; i < pivot; ++i)
		{
			textVec[i].setCharacterSize(textSize);
			textValVec[i].setCharacterSize(textSize);

			textVec[i].setPosition(forEach + 7, i*stepper);
			textValVec[i].setPosition(((2*(offset+5) + 10)/2 - textValVec[i].getGlobalBounds().width - 1),
										i*stepper);
		}

		for(int i = pivot; i < textVec.size(); ++i)
		{
			textVec[i].setCharacterSize(textSize);
			textValVec[i].setCharacterSize(textSize);
			
			textVec[i].setPosition(((2*(offset+5) + 10)/2 + sprVec[i].getGlobalBounds().width + 4), (i-pivot)*stepper);
			textValVec[i].setPosition((2*(offset+5)) - textValVec[i].getGlobalBounds().width,
										(i-pivot)*stepper);
		}
		
		goBack.setCharacterSize(textSize);
		goBack.setPosition(parent.getSize().x/2 - (goBack.getGlobalBounds().width)/2,
							parent.getSize().y - goBack.getGlobalBounds().height - 20);
	}

	parent.setActive(true);

	while(parent.isOpen())
	{
		auto pos = sf::Mouse::getPosition(parent);
		sf::Event e;
		while(parent.pollEvent(e))
		{
			if (e.type == e.Closed || e.type == e.KeyPressed)
			{
				if (e.type == e.KeyPressed && !(e.key.code == sf::Keyboard::Escape))	continue;
				parent.create(vMode, name, mode);
				return;
			}
			else if (e.type == e.MouseButtonPressed)
			{
				if (goBack.getGlobalBounds().contains(pos.x, pos.y))
				{
					parent.create(vMode, name, mode);
					return;
				}
			}
		}
		auto bgColor = sf::Color(4, 18, 27);
		parent.clear(bgColor);
		for(auto&& a : textVec)
			parent.draw(a);

		for(auto&& a : textValVec)
		{
			parent.draw(a);
		}

		for(auto&& a : sprVec)
			parent.draw(a);

		parent.draw(goBack);
		parent.display();
	}
	parent.create(vMode, name, mode);
}

void showWindowsDispatch(std::vector<SkillData>& vec, sf::RenderWindow& window, sf::VideoMode vMode, \
														const std::string& name, sf::Uint32 style, int panelH)
{
	sf::Font font;
	font.loadFromFile("fonts/runescape_chat_font.ttf");

	std::vector<sf::Text> options;
	{
		sf::Text text;
		text.setFont(font);
		text.setColor(sf::Color::Yellow);
		text.setString("Order by Xps Remaining");
		auto bounds = text.getGlobalBounds();
		text.setPosition((300 - bounds.width)/2, 5);

		options.push_back(text);
	}

	{
		sf::Text text;
		text.setFont(font);
		text.setColor(sf::Color::White);
		text.setString("Order by Current Xps");
		auto bounds = text.getGlobalBounds();
		text.setPosition((300 - bounds.width)/2, 45);

		options.push_back(text);
	}
	
	{
		sf::Text text;
		text.setFont(font);
		text.setColor(sf::Color::Yellow);
		text.setString("Order by Current Rank");
		auto bounds = text.getGlobalBounds();
		text.setPosition((300 - bounds.width)/2, 85);

		options.push_back(text);
	}

	{
		sf::Text text;
		text.setFont(font);
		text.setColor(sf::Color::White);
		text.setString("Order by Xps till 200m");
		auto bounds = text.getGlobalBounds();
		text.setPosition((300 - bounds.width)/2, 125);
		options.push_back(text);
	}

	sf::Text goBack;
	goBack.setFont(font);
	goBack.setColor(sf::Color::Yellow);
	goBack.setString("Go Back");
	auto goBackBounds = goBack.getGlobalBounds();
	goBack.setPosition((300 - goBackBounds.width)/2, 165);

	window.create(sf::VideoMode(300, 220), "Which", sf::Style::Titlebar | sf::Style::Close);

	while(window.isOpen())
	{
		sf::Event e;
		while(window.pollEvent(e))
		{
			auto pos = sf::Mouse::getPosition(window);
			if (e.type == e.Closed || e.type == e.KeyPressed)
			{
				if (e.type == e.KeyPressed && !(e.key.code == sf::Keyboard::Escape))	continue;
				window.create(vMode, name, style);
				return;
			}
			else if (e.type == e.MouseButtonPressed)
			{
				if (goBack.getGlobalBounds().contains(pos.x, pos.y))
				{
					window.create(vMode, name, style);
					return;
				}
				for(int i = 0; i < options.size(); ++i)
				{
					if (options[i].getGlobalBounds().contains(pos.x, pos.y))
					{
						window.close();
						window.setActive(false);
						showWindowOrdered(vec, (showOption)i, window, sf::VideoMode(window.getSize().x, window.getSize().y),\
																"Which", sf::Style::Titlebar | sf::Style::Close,\
																	panelH);
					}
				}
			}
		}
		auto bgColor = sf::Color(4, 18, 27);
		window.clear(bgColor);
		window.draw(goBack);
		for(auto&& a : options)
		{
			window.draw(a);
		}
		window.display();
	}
	window.create(vMode, name, style);
}

#endif	//_OTHER_WINDOWS_H_