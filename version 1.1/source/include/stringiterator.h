/*

    stringiterator.h    -   made by Eduard Lahl

    This library contains class StringIterator, which is used for
    easy manipulation of strings.
    You can read certain number of elements from given string, stop
    on delimiters you set, read whole string at once and more.

    API:

        class StringIterator

            StringIterator(std::string& intialWords, std::string delimiters = "\n\t ")
                - default constructor which takes string that we want to store and
                  traverse and delimiter list which defaults to new line, tab and space

            StringIterator(StringIterator&& other)
                - move constructor

            StringIterator(std::string intialWords&&, std::string delimiters = "\n\t ")
                - default constructor which takes string that we want to store and
                  traverse and delimiter list which defaults to new line, tab and space

			void changeString(const std::string& s)
				- changes the internal container with letters from s
				- call to this member function resets the current position of iterator

			void changeString(std::string&& s)
				- changes the internal container with letters from s
				- call to this member function resets the current position of iterator
				
			void appendString(const std::string& toAppend, uint where = std::string::npos)
				- appends content from toAppend string to the specified position
				- if position is not passed, it appends the content to the end

			void appendString(std::string&& toAppend, uint where = std::string::npos)
				- appends content from toAppend string to the specified position
				- if position is not passed, it appends the content to the end

            void move(uint newPosition)
                - moves the iterator(first read character on next read or peek call)
                  into newPosition
                - if newPosition is out of range(smaller than 0 or bigger then
                  full string), it sets to either 0 or container.size()

            void restorePosition(uint listPosition = last_push)
                - restores position at given listPosition
                - if no parameters are passed, this function call
                  results in last stored position(before last read or readSpecial call)
                  being restored
                - if you restore to other position than last pushed, the list
                  is cleared from back until it has removed all trailing entries on list

            void changeDelimList(std::string&& newDelimList)
                - change delimiter list on run(runtime) to newDelimList

            uint getStringSize()
                - returns the size of the string passed as intialWords in container

            uint getPosSize()
                - returns number of entries in list of last positions

            uint getNextWordLength()
                - returns distance from current position of iterator until
                  first delimiter

            uin getNextFullWordLength()
                - returns distance from the last delimiter to next delimiter

            std::string read(uint howMany, bool stopOnDelimiter = true)
                - reads howMany characters starting at current position of iterator
                - if stopOnDelimiter is not passed or set to true, the read stops
                  on next delimiter or after howMany characters have been read
                  whichever comes first
                - if stopOnDelimiter is passed as false, this reads howMany characters
                  starting at current position of iterator or until end of string
                  has been found
                - call to this function will move iterator forwards

            std::string readSpecial(uint howMany, bool stopOnDelimiter = true)
                - reads howMany characters starting from the start of the word
                  that the iterator is over
                - the same other rules as for read apply

            std::string peek(uint HowMany, bool stopOnDelimiter = true)
                - does the same thing as read, but doesnt move iterator forwards

            std::string peekSpecial(uint HowMany, bool stopOnDelimiter = true)
                - does the same thing as peek, but doesnt move iterator forwards

			std::string rread(uint HowMany, bool stopOnDelimiter = true)
				- does the same thing as read does, but backwards

			std::string rreadSpecial(uint HowMany, bool stopOnDelimiter = true)
				- does the same thing as readSpecial does, but backwards

			std::string rpeek(uint HowMany, bool stopOnDelimiter = true)
				- does the same thing as peek does, but backwards

			std::string rpeekSpecial(uint HowMany, bool stopOnDelimiter = true)
				- does the same thing as peekSpecial does, but backwards

			std::pair<uint8, uint> getNextDelimiter()
				- returns a pair of <character, position> of next delimiter going
				  forwards on the stream
				- if there is none, returns [0, std::string::npos]
				- call to this member function will not move iterator

			std::pair<uint8, uint> getLastDelimiter()
				- returns a pair of <character, position> of next delimiter going
				  backwards on the stream
				- if there is none, returns [0, std::string::npos]
				- call to this member function will not move iterator

            inline bool end()
                - returns whether current position of iterator is at the
                  end of string or not

            inline bool start()
                - returns whether current position of iterator is at the
                  start of the string

            inline uint getPosition()
                - returns the current position of iterator as integral value

*/
#ifndef _STRING_ITERATOR_H_
#define _STRING_ITERATOR_H_

#include "side/basicutilsdll.h"
#include <vector>
#include <string>
#include <functional>
#include <algorithm>

namespace game{
	class StringIterator final{
		std::string container;
		std::string delim_list;
		uint currentPos;
		std::vector<uint> lastPos;
		const static uint last_push = -1;
		bool _lock;

		void _unlock_move_iterator()
		{
			_lock = false;
		}

		void _lock_move_iterator()
		{
			_lock = true;
		}

		//find if the char c is matching any chars from
		//delim_list(list of delimiters)
		bool _match_any_delimiter(const char c)
		{
			for(int i = delim_list.size()-1; i >= 0; --i)
			{
				if (delim_list[i] == c)	return true;
			}
			return false;
		}

		//starts at the currentPos, goes until the end of string
		//if not found, returns npos
		uint _find_closest_delim()
		{
			uint u = container.size();
			for(uint i = currentPos; i < u; ++i)
			{
				if (_match_any_delimiter(container[i]))
					return i;
			}
			return std::string::npos;
		}

		uint _reverse_find_closest_delim()
		{
			for(uint i = currentPos; i <= container.size()-1; --i)
			{
				if (_match_any_delimiter(container[i]))
					return i;
			}
			return std::string::npos;
		}
		StringIterator(const StringIterator&);
		StringIterator& operator=(const StringIterator&);
	public:

		StringIterator() : container(), delim_list(),\
						currentPos(), lastPos(), _lock(false)
		{
		}

		//default constructor
		StringIterator(std::string& intialWords, std::string delimiters = "\n\t ") :\
						container(intialWords), delim_list(delimiters),\
						currentPos(), lastPos(),\
						_lock(false)
		{
		}

		//move construct
		StringIterator(StringIterator&& other) : container(std::move(other.container)),\
						delim_list(std::move(other.delim_list)),\
						currentPos(std::move(other.currentPos)),\
						lastPos(std::move(other.lastPos)),\
						_lock(std::move(other._lock))
		{
		}

		StringIterator(std::string&& intialWords, std::string delimiters = "\n\t ") :\
						container(intialWords), delim_list(delimiters),\
						currentPos(), lastPos(),\
						_lock(false)
		{
		}

		//changes the content of container with characters from s
		//call to this member function resets current position of iterator
		void changeString(const std::string& s)
		{
			container = s;
			currentPos = 0;
		}

		//changes the content of container with characters from s
		//call to this member function resets current position of iterator
		void changeString(std::string&& s)
		{
			std::swap(container, s);
			currentPos = 0;
		}

		void appendString(const std::string& toAppend, uint where = std::string::npos)
		{
			if (where != std::string::npos)
				container.insert(container.begin()+where, toAppend.begin(), toAppend.end());
			else
				container.insert(container.end(), toAppend.begin(), toAppend.end());
		}

		void appendString(std::string&& toAppend, uint where = std::string::npos)
		{
			appendString(toAppend, where);
		}

		//move the position to new place
		void move(uint newPos)
		{
			lastPos.push_back(currentPos);
			if (newPos < container.size()-1)
				if (newPos <= 0)	currentPos = 0;
				else				currentPos = newPos;
			else
				currentPos = container.size()-1;
		}

		//point must be in range of [0, size of list)
		//if left empty, restores to position before last move call
		//if its not left empty or not equal to its value(-1), the
		//currentPos is restored to point-th position and all positions
		//after point are removed from the list
		void restorePosition(uint point = last_push)
		{
			if (point == last_push)
			{
				uint a = lastPos.at(lastPos.size()-1);
				lastPos.pop_back();
				std::swap(currentPos, a);
			}
			else
			{
				currentPos = lastPos[point];
				lastPos.erase(lastPos.begin()+point, lastPos.end());
			}
		}

		//change the list of letters that are considered delimiters
		void changeDelimList(std::string&& newDelimList)
		{
			std::swap(newDelimList, delim_list);
		}

		//get the size of the container string
		uint getStringSize()
		{
			return container.size();
		}

		//get the size of list of positions(useful for
		//restorePosition)
		uint getPosSize()
		{
			return lastPos.size();
		}

		//returns the distance from current position to next delimiter
		uint getNextWordLength()
		{
			return peek(std::string::npos).size();
		}

		//returns length of the next word even if you are in the
		//middle of it
		uint getNextFullWordLength()
		{
			return peekSpecial(std::string::npos).size();
		}

		//reads "howMany" characters from the string
		//if stopOnDelimiter is not set or set to true, the reading
		//stops at the next delimiter
		//if false is passed as second argument, it reads exactly howMany letters
		//call to this function does move the iterator forward
		std::string read(uint howMany, bool stopOnDelimiter = true)
		{
			if (!start()) ++currentPos;
			bool isLocked = _lock;
			//try to unlock the lock that prevents the iterator position to move
			_unlock_move_iterator();
			//if already at end
			if (end())
			{
				//return empty string
				return std::string("");
			}
			//find closest delimiter
			uint pos = _find_closest_delim();

			//if howMany is npos(-1), or howMany is higher then there is empty things in
			//list
			if (howMany == std::string::npos || howMany + currentPos >= container.size()-1)
				//for peekSpecial etc
			{
				std::string s;
				uint moveTo = 0;
				s.reserve(container.size() - currentPos);
				//if stopOnDelimiter and next delimiter exists
				if (stopOnDelimiter && !(pos == std::string::npos))
				{
					//insert until position of next delimiter
					s.insert(s.begin(), container.begin()+currentPos, container.begin()+pos);
					//update dummy
					moveTo += pos;
				}
				//if stopOnDelimiter is false or there is no other delimiter
				//(the higher if makes sure that user reqests more characters than
				//there is left in stream)
				else
				{
					//insert everything left into string
					s.insert(s.begin(), container.begin()+currentPos, container.end());
					//update dummy
					moveTo = container.size()-1;
				}
				//if not locked
				if (!isLocked)
				{
					//push current position to list of positions
					lastPos.push_back(currentPos);
					//update current position
					currentPos = moveTo;
				}
				//shrink the string to prevent potentional unsued memory
				s.shrink_to_fit();
				//return string
				return s;
			}
			//this branch is taken when user requested less characters than
			//there are left in stream
			std::string s;
			s.reserve(howMany);
			//if dont stop on next delimiter
			if (!stopOnDelimiter)
			{
				//insert howMany characters into string
				s.insert(s.begin(), container.begin()+currentPos, container.begin()+howMany+currentPos);
				//if not locked
				if (!isLocked)
				{
					//push current position to list of positions
					lastPos.push_back(currentPos);
					//update current position
					currentPos = pos;
				}
				return s;
			}
			//if we want to stop on next delimiter
			//insert characters up until next delimiter or until howMany(whichever comes first)
			if (pos-currentPos < howMany)
				s.insert(s.begin(), container.begin()+currentPos, container.begin()+pos);
			else
				s.insert(s.begin(), container.begin()+currentPos, container.begin()+howMany+currentPos);
			if (!isLocked)
			{
				//push current position to list of positions
				lastPos.push_back(currentPos);
				//update current position
				currentPos = pos;
			}
			//shrink the string to prevent potentional unsued memory
			s.shrink_to_fit();
			//return string
			return s;
		}
		
		//reads "howMany" characters from the string from the beginning of the
		//last word(if stopped at the middle of word, it moves backwards until it
		//finds another dimiter)
		//if stopOnDelimiter is not set or set to true, the reading
		//stops at the next delimiter
		//if false is passed as second argument, it reads exactly howMany letters
		//call to this function does move the iterator forward
		std::string readSpecial(uint howMany, bool stopOnDelimiter = true)
		{
			if (currentPos == 0)	read(howMany, stopOnDelimiter);
			while(!_match_any_delimiter(container[currentPos])) { --currentPos; }
			return read(howMany, stopOnDelimiter);
		}
		
		//reads "howMany" characters from the string
		//if stopOnDelimiter is not set or set to true, the reading
		//stops at the next delimiter
		//if false is passed as second argument, it reads exactly howMany letters
		//call to this function does not move the iterator forward
		std::string peek(uint howMany, bool stopOnDelimiter = true)
		{
			//lock the moving
			_lock_move_iterator();
			//read howMany characters
			return read(howMany, stopOnDelimiter);
		}

		//reads "howMany" characters from the string from the beginning of the
		//last word(if stopped at the middle of word, it moves backwards until it
		//finds another dimiter)
		//if stopOnDelimiter is not set or set to true, the reading
		//stops at the next delimiter
		//if false is passed as second argument, it reads exactly howMany letters
		//call to this function does not move the iterator forward
		std::string peekSpecial(uint howMany, bool stopOnDelimiter = true)
		{
			if (currentPos == 0)	peek(howMany, stopOnDelimiter);
			while(!_match_any_delimiter(container[currentPos])) { --currentPos; }
			return peek(std::string::npos, stopOnDelimiter);
		}

		//does the same thing as read member function, but it does it backwards
		std::string rread(uint howMany, bool stopOnDelimiter = true)
		{
			if (!end()) --currentPos;
			bool isLocked = _lock;
			//try to unlock the lock that prevents the iterator position to move
			_unlock_move_iterator();
			//if already at start
			if (start())
			{
				//return empty string
				return std::string("");
			}
			//find closest delimiter
			uint pos = _reverse_find_closest_delim();

			//if howMany is npos(-1), or howMany is higher then there is empty things in
			//list
			if (howMany == std::string::npos || howMany + currentPos <= 0)
				//for rpeekSpecial etc
			{
				std::string s;
				uint moveTo = 0;
				s.reserve(currentPos);
				//if stopOnDelimiter and next delimiter exists
				if (stopOnDelimiter && !(pos == std::string::npos))
				{
					//insert until position of next delimiter
					s.insert(s.begin(), container.rbegin()+container.size()-currentPos-1, container.rbegin()+container.size()-pos-1);
					//update dummy
					moveTo += pos;
				}
				//if stopOnDelimiter is false or there is no other delimiter
				//(the higher if makes sure that user reqests more characters than
				//there is left in stream)
				else
				{
					//insert everything left into string
					s.insert(s.begin(), container.rbegin()+container.size()-currentPos-1, container.rend());
					//update dummy
					moveTo = 0;
				}
				//if not locked
				if (!isLocked)
				{
					//push current position to list of positions
					lastPos.push_back(currentPos);
					//update current position
					currentPos = moveTo;
				}
				//shrink the string to prevent potentional unsued memory
				s.shrink_to_fit();
				//return string
				return s;
			}
			//this branch is taken when there is next delimiter and user requested less
			//characters than there are left in stream
			std::string s;
			s.reserve(howMany);
			//if dont stop on next delimiter
			if (!stopOnDelimiter)
			{
				//insert howMany characters into string
				s.insert(s.begin(), container.rbegin()+container.size()-currentPos,\
						container.rbegin()+container.size()-howMany-currentPos);
				//if not locked
				if (!isLocked)
				{
					//push current position to list of positions
					lastPos.push_back(currentPos);
					//update current position
					currentPos = pos;
				}
				return s;
			}
			//if we want to stop on next delimiter
			//insert characters up until next delimiter or until howMany(whichever comes first)
			if (pos-currentPos < howMany)
				s.insert(s.begin(), container.rbegin()+container.size()-currentPos,\
							container.rbegin()+container.size()-pos);
			else
				s.insert(s.begin(), container.rbegin()+container.size()-currentPos,\
							container.rbegin()+container.size()-howMany-currentPos);
			if (!isLocked)
			{
				//push current position to list of positions
				lastPos.push_back(currentPos);
				//update current position
				currentPos = pos;
			}
			//shrink the string to prevent potentional unsued memory
			s.shrink_to_fit();
			//return string
			return s;
		}

		//does the same thing as readSpecial member function but does it backwards
		std::string rreadSpecial(uint howMany, bool stopOnDelimiter = true)
		{
			if (currentPos >= container.size()-1)	rread(howMany, stopOnDelimiter);
			while(!_match_any_delimiter(container[currentPos])) { ++currentPos; }
			return rread(howMany, stopOnDelimiter);
		}

		//does the same thing as peek member function, but backwards
		std::string rpeek(uint howMany, bool stopOnDelimiter = true)
		{
			//lock the moving
			_lock_move_iterator();
			//read howMany characters
			return rread(howMany, stopOnDelimiter);
		}

		//does the same thing as peekSpecial member function, but backwardsbut backwards
		std::string rpeekSpecial(uint howMany, bool stopOnDelimiter = true)
		{
			if (currentPos >= container.size()-1)	rpeek(howMany, stopOnDelimiter);
			while(!_match_any_delimiter(container[currentPos])) { ++currentPos; }
			return rpeek(std::string::npos, stopOnDelimiter);
		}

		//returns the next delimiter that will be found on the string in pair of
		//<the character, position>
		std::pair<uint8, uint> getNextDelimiter()
		{
			auto cPos = currentPos;
			for(uint u = cPos; u < container.size(); ++u)
				if (_match_any_delimiter(container[u]))
					return std::pair<uint8, uint>(container[u], u);

			return std::pair<uint8, uint>(0, std::string::npos);
		}

		//returns the last(next if calling reverse member functions) delimiter
		//that was(will be) found in pair of <the character, position>
		std::pair<uint8 , uint> getLastDelimiter()
		{
			auto cPos = currentPos;
			for(uint u = cPos; u < container.size(); --u)
				if (_match_any_delimiter(container[u]))
					return std::pair<uint8, uint>(container[u], u);

			return std::pair<uint8, uint>(0, std::string::npos);
		}

		//return if the iterator is at the end
		inline bool end()	{ return currentPos >= (container.size()); }

		//return if the iterator is at the start
		inline bool start() { return currentPos <= 0; }

		//return current position of iterator as integral value
		inline uint getPosition() { return currentPos; }
	};
};

#endif	//_STRING_ITERATOR_H_