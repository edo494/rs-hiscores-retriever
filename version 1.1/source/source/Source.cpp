#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS

#ifdef DLL_EXPORT
#define EXTERN_API __declspec(dllexport)
#else
#define EXTERN_API __declspec(dllimport)
#endif

#include <atomic>

#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <string>
#include <thread>

#include "stringiterator.h"
#include "side/encryptedfilecdll.h"

#include "side/utils.h"
#include "side/skilldata.h"
#include "side/otherWindows.h"
#include "side/confreader.h"

//				  01.01.00
const int version = 10100;

std::string establishConnection()
{
	static bool first = false;
	bool OK = false;
	sf::Http http;
	http.setHost("http://hiscore.runescape.com/");
	sf::Http::Response response;
	while(!OK)
	{
		sf::Http::Request req;
		std::string reqStr = "/index_lite.ws?player=";
		std::string name;
		if (first)
			std::cout << "\n";
		else
			first = true;

		std::cout << "Please insert RS nickname to lookup: ";
		std::getline(std::cin, name);
		reqStr.append(name);
		req.setUri(reqStr);
		req.setMethod(sf::Http::Request::Method::Get);
		response = http.sendRequest(req);
		if (response.getStatus() == response.Ok)
			OK = true;
		else
			std::cout << "\nUser not found in Hiscores, please try again!\n";
	}
	return response.getBody();
}


void check_updates()
{
	sf::Http session("http://rshiscoreretriever.wz.sk");
	sf::Http::Request req;
	req.setUri("/verCheck.html");
	auto resp = session.sendRequest(req);
	std::string whenNew = "\nNewer version is available. Run auto-updater to get newest version.\n";
	if (resp.getStatus() == resp.Ok)
	{
		std::string response = resp.getBody();
		game::StringIterator iter(response, ".");
		std::string read;
		
		read = iter.read(-1);
		int major = convert(read);

		read = iter.read(-1);
		int minor = convert(read);

		read = iter.read(-1);
		int small = convert(read);

		int final = major * 10000 + minor * 100 + small;
		if (final > version)
		{
			std::cout << whenNew;
			return;
		}
	}
	else
	{
		std::cout << "Failed to check version!\n";
	}
}

void readConfig(int& panelHeight)
{
	{
		game::FileManipC tempFile("config.conf");
		if (!tempFile.size())
		{
			tempFile.removeFile();
			tempFile.close();
		}
	}
	ConfigReader reader("config.conf");

	std::string pHeight = reader.read("Windows-Panel-Height");
	panelHeight = convert(pHeight);
}

int main()
{

	const sf::Color Orange = sf::Color(0xff, 140, 0);
	const sf::Color TooltipColor = sf::Color::Yellow;

	std::vector<int> xps;	
	loadXps(xps);

	check_updates();

	int panelHeight = 0;

	readConfig(panelHeight);

	if (!panelHeight)
		panelHeight = 46;

	while(1)
	{
		std::string responseStr = establishConnection();

		game::StringIterator iter(responseStr, "\n,\t");
		std::string parser;

		std::vector<SkillData> data;

		for(int i = 0; i < 27; ++i)
		{
			SkillData dat;
			//read rank
			parser = iter.read(-1);
			auto rank = convert(parser);
			dat.rank = rank;

			//read level
			parser = iter.read(-1);
			auto level = convert(parser);
			dat.lvl = level;

			//read xps
			parser = iter.read(-1);
			long long xpsSkill = convert64(parser);
			dat.xps = xpsSkill;

			dat.name = index2String(i);

			data.push_back(dat);
		}

		sf::Font font;
		font.loadFromFile("fonts/runescape_chat_font.ttf");

		std::vector<sf::IntRect> coords;
		loadCoords(coords);

		std::vector<int> xpsNextLvl;
		xpsNextLvl.push_back(0);

		auto xpsTillMax = 0;
		auto xpsTillComp = 0;
	
		auto lvlTillMax = 0;
		auto lvlTillComp = 0;

		long long xpsTotalMax = 0;

		for(int i = 1, j = data.size(); i < j; ++i)
		{
			if (data[i].lvl == -1 || data[i].rank == -1)
			{
				xpsNextLvl.push_back(xps[1]);
				xpsTillMax += (xps[98]);
				lvlTillMax += 98;
				xpsTillComp += (xps[98]);
				lvlTillComp += 98;
				if (i == findIndexNames("Dungeoneering"))
				{
					xpsTillComp += (xps[119] - xps[98]);
					lvlTillComp += (119 - 98);
				}
			}
			else if (data[i].lvl == 120)
			{
				xpsNextLvl.push_back(0);
			}
			else if (i != findIndexNames("Dungeoneering") && data[i].xps >= xps[98])
			{
				xpsNextLvl.push_back(0);
			}
			else
			{
				auto lvl = data[i].lvl;
				auto xpsLeft = xps[lvl] - data[i].xps;
				auto a = findIndexNames("Dungeoneering");
				if (i == a)
				{
					xpsTillComp += (xps[119] - data[i].xps);
					lvlTillComp += (120 - lvl);
					if (!(lvl >= 99))
					{
						xpsTillMax += (xps[98] - data[i].xps);
						lvlTillMax += (99 - lvl);
					}
				}
				else
				{
					xpsTillComp += (xps[98] - data[i].xps);
					lvlTillComp += (99 - data[i].lvl);
					xpsTillMax += (xps[98] - data[i].xps);
					lvlTillMax += (99 - data[i].lvl);
				}
				xpsNextLvl.push_back(xpsLeft);
			}
		}

		std::vector<int> xps200;
		xps200.push_back(0);
		for(int i = 1, j = data.size(); i < j; ++i)
		{
			xps200.push_back(200000000 - data[i].xps);
		}

		for(auto&& fVal : xps200)
		{
			xpsTotalMax += fVal;
		}

		auto lambda = [](const std::string& path) { std::cout << "error while loading " << path << "\n"; };
	
		sf::Texture tex;
		loadEncryptedFile("pictures/stats.pngcyp", tex, lambda);
		sf::Sprite spr;
		spr.setTexture(tex);
		spr.scale(1.23f, 1.23f);
		auto texSizeX = tex.getSize().x*1.23f + 512;
		auto texSizeY = tex.getSize().y*1.23f;
		sf::RenderWindow window(sf::VideoMode(tex.getSize().x*1.23f + 512, tex.getSize().y*1.23f), "Hiscores Retriever", sf::Style::Titlebar | sf::Style::Close);

		auto resolution = sf::VideoMode::getDesktopMode();

		if ((window.getSize().x > resolution.width) || (window.getSize().y > resolution.height))
		{
			window.close();
			std::cout << "Resolution is too small!\n";
			std::cin.get();
			return 1;
		}

		sf::Text currentSText;
		bool display = false;
		const int iconSize = 58;

		currentSText.setFont(font);
		currentSText.setPosition((269+58+10), (8+58/2-20));
		currentSText.setColor(Orange);
		currentSText.setString("Total Level");

		const sf::Vector2f sprite1Pos(269, 8);
		const sf::Vector2f sprite2Pos(500, 8);
		const auto spriteDiff = sprite2Pos - sprite1Pos;

		std::vector<sf::Texture> skillTex;
		std::vector<sf::Sprite> skillSpr;
		loadSkillIcons(skillTex, skillSpr);

		for(int i = 0; i < skillSpr.size(); ++i)
		{
			scaleSprite(iconSize, iconSize, skillSpr[i]);
		}


		{
			auto textRect = currentSText.getGlobalBounds();
			auto intialPosDiff = (spriteDiff.x - iconSize - textRect.width)/2.0f;
			intialPosDiff += iconSize;
			intialPosDiff += sprite1Pos.x;
			currentSText.setPosition(intialPosDiff, currentSText.getPosition().y);
		}

		std::pair<sf::Sprite, sf::Sprite> currentIcons;
		currentIcons.first = skillSpr[0];
		currentIcons.second = skillSpr[0];
		currentIcons.first.setPosition(sprite1Pos);
		currentIcons.second.setPosition(sprite2Pos);

		//xps currently in skill
		sf::Text xpsText;
		xpsText.setFont(font);
		xpsText.setColor(TooltipColor);
		xpsText.setString("Xps:");
		xpsText.setPosition(275, 127);

		sf::Text xpsValText;
		xpsValText.setFont(font);
		xpsValText.setColor(TooltipColor);
		xpsValText.setString(format(xpsNextLvl[0]));
		xpsValText.setPosition(400, 127);

	
		//current level in skill
		sf::Text currentLevelText;
		currentLevelText.setFont(font);
		currentLevelText.setColor(TooltipColor);
		currentLevelText.setString("Level:");
		currentLevelText.setPosition(275, 87);

		sf::Text currentLevelValText;
		currentLevelValText.setFont(font);
		currentLevelValText.setColor(TooltipColor);
		currentLevelValText.setString(format(xpsTillComp));
		currentLevelValText.setPosition(400, 87);

	
		//current rank
		sf::Text currentRankText;
		currentRankText.setFont(font);
		currentRankText.setColor(TooltipColor);
		currentRankText.setString("Rank:");
		currentRankText.setPosition(275, 167);

		sf::Text currentRankValText;
		currentRankValText.setFont(font);
		currentRankValText.setColor(TooltipColor);
		currentRankValText.setString(format(xpsTillComp));
		currentRankValText.setPosition(400, 167);


		//current xps left(either in skill or for maximum total)
		sf::Text xpsLeftTotalText;
		xpsLeftTotalText.setFont(font);
		xpsLeftTotalText.setColor(TooltipColor);
		xpsLeftTotalText.setString("Until Max:");
		xpsLeftTotalText.setPosition(275, 207);

		sf::Text xpsLeftTotalValText;
		xpsLeftTotalValText.setFont(font);
		xpsLeftTotalValText.setColor(TooltipColor);
		xpsLeftTotalValText.setString(format(xpsTillComp));
		xpsLeftTotalValText.setPosition(400, 207);


		sf::Text expectedTtlXpsText;
		expectedTtlXpsText.setFont(font);
		expectedTtlXpsText.setColor(TooltipColor);
		expectedTtlXpsText.setString("Xps at Max:");
		expectedTtlXpsText.setPosition(275, 247);

		sf::Text expectedTtlXpsValText;
		expectedTtlXpsValText.setFont(font);
		expectedTtlXpsValText.setColor(TooltipColor);
		expectedTtlXpsValText.setString(format(data[0].xps + xpsTillComp));
		expectedTtlXpsValText.setPosition(400, 247);


		sf::Text reEnter;
		reEnter.setFont(font);
		reEnter.setString("New Name");
		reEnter.setColor(sf::Color::White);
		reEnter.setPosition(620, 6);

		sf::Text order;
		order.setFont(font);
		order.setString("Order Skills");
		order.setColor(sf::Color::White);
		order.setPosition(620, 46);

		sf::Text ttlLvl;
		ttlLvl.setFont(font);
		ttlLvl.setString(std::to_string(data[0].lvl));
		ttlLvl.setCharacterSize(21);
		ttlLvl.setPosition(123, 338);
		ttlLvl.setColor(sf::Color::White);

		std::vector<sf::Text> firstSkillVec;
		std::vector<sf::Text> secondSkillVec;

		for(int i = 1; i < coords.size(); ++i)
		{
			sf::Text first;
			sf::Text second;
			first.setFont(font);
			second.setFont(font);
			
			first.setCharacterSize(16);
			second.setCharacterSize(16);
			
			first.setColor(Orange);
			second.setColor(Orange);

			auto coord = coords[i];
			first.setPosition(coord.left + (45 - coords[1].left), coord.top + (2 - coords[1].top));
			second.setPosition(coord.left + (62 - coords[1].left), coord.top + (15 - coords[1].top));

			if (first.getPosition().x > 160)
			{
				first.move(-2, 0);
				second.move(-2, 0);
			}

			first.setString(std::to_string(data[i].lvl));
			second.setString(std::to_string(data[i].lvl));
			
			firstSkillVec.push_back(first);
			secondSkillVec.push_back(second);
		}

		int currentShown = 0;
		bool showAtMax = false;

		window.setVerticalSyncEnabled(true);

		while(window.isOpen())
		{
			auto pos = sf::Mouse::getPosition(window);
			sf::Event ev;
			while(window.pollEvent(ev))
			{
				if (ev.type == ev.Closed || ev.type == ev.KeyPressed)
				{
					if (ev.type == ev.KeyPressed && !(ev.key.code == sf::Keyboard::Escape))	continue;
					exit(0);
				}

				else if (ev.type == ev.MouseButtonPressed)
				{
					if (order.getGlobalBounds().contains(pos.x, pos.y))
					{
						showWindowsDispatch(data, window, sf::VideoMode(window.getSize().x, window.getSize().y),\
							"Hiscores Retriever", sf::Style::Titlebar | sf::Style::Close, panelHeight);
					}

					else if (reEnter.getGlobalBounds().contains(pos.x, pos.y))
					{
						window.close();
						break;
					}
				}
			}

			bool found = false;
			for(int i = 0; i < coords.size(); ++i)
			{
				if (coords[i].contains(pos.x, pos.y))
				{
					found = true;
					if (currentShown == i)	break;

					currentShown = i;
					currentSText.setString(index2String(i));
					auto textRect = currentSText.getGlobalBounds();
					auto intialPosDiff = (spriteDiff.x - iconSize - textRect.width)/2.0f + iconSize + sprite1Pos.x;
					currentSText.setPosition(intialPosDiff, currentSText.getPosition().y);

					currentIcons.first = skillSpr.at(i);
					currentIcons.second = skillSpr.at(i);
					currentIcons.first.setPosition(sprite1Pos);
					currentIcons.second.setPosition(sprite2Pos);

					currentLevelValText.setString(std::to_string(data[i].lvl));
					auto bounds = currentLevelValText.getGlobalBounds();
					currentLevelValText.setPosition(600 - bounds.width, currentLevelValText.getPosition().y);

					xpsValText.setString(format(data[i].xps));
					bounds = xpsValText.getGlobalBounds();
					xpsValText.setPosition(600 - bounds.width, xpsValText.getPosition().y);

					currentRankValText.setString(format(data[i].rank));
					bounds = currentRankValText.getGlobalBounds();
					currentRankValText.setPosition(600 - bounds.width, currentRankValText.getPosition().y);

					if (i)
					{
						std::string xpsLNow;
						if ((data[i].lvl == 99 && i != findIndexNames("Dungeoneering")) || data[i].lvl == 120)
						{
							xpsLNow = "Until 200m:";

							xpsLeftTotalValText.setString(format(xps200[i]));
							bounds = xpsLeftTotalValText.getGlobalBounds();
							xpsLeftTotalValText.setPosition(600 - bounds.width, xpsLeftTotalValText.getPosition().y);

							showAtMax = false;
						}
						else
						{
							xpsLNow = "Next Level:";

							xpsLeftTotalValText.setString(format(xpsNextLvl[i]));
							bounds = xpsLeftTotalValText.getGlobalBounds();
							xpsLeftTotalValText.setPosition(600 - bounds.width, xpsLeftTotalValText.getPosition().y);

								
							showAtMax = true;
							std::string xpsAtMax = "Xps until 99:";
							if (i != findIndexNames("Dungeoneering") || data[i].lvl < 99)
							{
								expectedTtlXpsText.setString(xpsAtMax);

								expectedTtlXpsValText.setString(format(xps[98] - data[i].xps));
								bounds = expectedTtlXpsValText.getGlobalBounds();
								expectedTtlXpsValText.setPosition(600 - bounds.width, expectedTtlXpsValText.getPosition().y);
							}
							else
							{
								expectedTtlXpsText.setString("Xps until 120:");

								expectedTtlXpsValText.setString(format(xps[119] - data[i].xps));
								bounds = expectedTtlXpsValText.getGlobalBounds();
								expectedTtlXpsValText.setPosition(600 - bounds.width, expectedTtlXpsValText.getPosition().y);
								if (data[i].lvl == 120)
									showAtMax = false;
							}
						}
						xpsLeftTotalText.setString(xpsLNow);
					}
					else
					{
						std::string xpsLNow = "Until Max:";
						xpsLeftTotalText.setString(xpsLNow);
								
						xpsLeftTotalValText.setString(format(xpsTillComp));
						bounds = xpsLeftTotalValText.getGlobalBounds();
						xpsLeftTotalValText.setPosition(600 - bounds.width, xpsLeftTotalValText.getPosition().y);
							
						showAtMax = true;
						std::string xpsAtMax = "Xps at Max:";
						expectedTtlXpsText.setString(xpsAtMax);

						expectedTtlXpsValText.setString(format(data[0].xps + xpsTillComp));
						bounds = expectedTtlXpsValText.getGlobalBounds();
						expectedTtlXpsValText.setPosition(600 - bounds.width, expectedTtlXpsValText.getPosition().y);
					}
				}
			}
			
			if (!found)
			{

				currentShown = 0;

				currentIcons.first = skillSpr.at(0);
				currentIcons.second = skillSpr.at(0);
				currentIcons.first.setPosition(sprite1Pos);
				currentIcons.second.setPosition(sprite2Pos);
				
				currentSText.setString(index2String(0));
				auto textRect = currentSText.getGlobalBounds();
				auto intialPosDiff = (spriteDiff.x - iconSize - textRect.width)/2.0f + iconSize + sprite1Pos.x;
				currentSText.setPosition(intialPosDiff, currentSText.getPosition().y);


				currentLevelValText.setString(std::to_string(data[0].lvl));
				auto bounds = currentLevelValText.getGlobalBounds();
				currentLevelValText.setPosition(600 - bounds.width, currentLevelValText.getPosition().y);

				xpsValText.setString(format(data[0].xps));
				bounds = xpsValText.getGlobalBounds();
				xpsValText.setPosition(600 - bounds.width, xpsValText.getPosition().y);

				currentRankValText.setString(format(data[0].rank));
				bounds = currentRankValText.getGlobalBounds();
				currentRankValText.setPosition(600 - bounds.width, currentRankValText.getPosition().y);

				xpsLeftTotalText.setString("Until Max:");
				xpsLeftTotalValText.setString(format(xpsTillComp));
				bounds = xpsLeftTotalValText.getGlobalBounds();
				xpsLeftTotalValText.setPosition(600 - bounds.width, xpsLeftTotalValText.getPosition().y);

				expectedTtlXpsText.setString("Xps at Max:");
				expectedTtlXpsValText.setString(format(data[0].xps + xpsTillComp));
				bounds = expectedTtlXpsValText.getGlobalBounds();
				expectedTtlXpsValText.setPosition(600 - bounds.width, expectedTtlXpsText.getPosition().y);
			}

			auto darkBlue = sf::Color(4, 18, 27);
			window.clear(darkBlue);
			window.draw(reEnter);
			window.draw(order);
			window.draw(spr);
			window.draw(currentSText);
			window.draw(xpsText);
			window.draw(xpsValText);
			window.draw(currentLevelText);
			window.draw(currentLevelValText);

			auto showTotal = !currentShown;
			auto level = data[currentShown].lvl;
			auto alreadyMaxXps = data[currentShown].xps == 2e8;

			if ((showTotal && level != 2595) || (!alreadyMaxXps && currentShown))
			{
				window.draw(xpsLeftTotalText);
				window.draw(xpsLeftTotalValText);
			}
			window.draw(currentRankText);
			window.draw(currentRankValText);
			window.draw(currentIcons.first);
			window.draw(currentIcons.second);
			if (showAtMax || !currentShown)
			{
				if (data[0].lvl != 2595)
				{
					window.draw(expectedTtlXpsText);
					window.draw(expectedTtlXpsValText);
				}
			}

			if (!currentShown)
			{
				//display Total Level stuff:
			}
			
			for(auto&& a : firstSkillVec)
				window.draw(a);

			for(auto&& a : secondSkillVec)
				window.draw(a);

			window.draw(ttlLvl);

			window.display();
		}
	}
}