This is a "almost"(see note 1) open source program that parses data read from
Runescape Hiscores and performs various manipulations on the data.

This program shows you graphical representation of your skills and is also capable of
sorting them by
	- Xps remaining to 99(120 dungeoneering)
	- Current Rank
	- Xps left to 200m xps
	- Xps in the skill
	
This program is powered by SFML(Simple Fast Multimedia Library) wrote by Laurent Gomila
(link: https://github.com/LaurentGomila/SFML )

Note 1:
	- all source is available, but the content of encryption.dll, which uses algorithm that
	  encrypts and decrypts the files and is meant to be kept private.
	  
Link to full version: https://bitbucket.org/edo494/rs-hiscores-retriever/downloads/Rs_hiscores_retriever.rar